﻿using RicksGuitars.Enums;

namespace RicksGuitars
{
    public class Guitar
    {
        private readonly string serialNumber, model;

        private readonly Builder builder;

        private readonly GuitarType type;

        private readonly Wood backWood, topWood;

        private decimal price;

        public Guitar(string serialNumber, decimal price,
                Builder builder, string model, GuitarType type,
                Wood backWood, Wood topWood)
        {
            this.serialNumber = serialNumber;
            this.price = price;
            this.builder = builder;
            this.model = model;
            this.type = type;
            this.backWood = backWood;
            this.topWood = topWood;
        }

        public string GetSerialNumber()
        {
            return serialNumber;
        }

        public decimal GetPrice()
        {
            return price;
        }

        public void SetPrice(decimal newPrice)
        {
            this.price = newPrice;
        }

        public Builder GetBuilder()
        {
            return builder;
        }

        public string GetModel()
        {
            return model;
        }

        public GuitarType GetGuitarType()
        {
            return type;
        }

        public Wood GetBackWood()
        {
            return backWood;
        }

        public Wood GetTopWood()
        {
            return topWood;
        }
    }
}
