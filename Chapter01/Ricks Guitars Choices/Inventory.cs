﻿using RicksGuitars.Enums;

namespace RicksGuitars
{
    public class Inventory
    {
        private readonly List<Guitar> guitars;

        public Inventory()
        {
            guitars = new List<Guitar>();
        }


        public void AddGuitar(string serialNumber, decimal price,
                      Builder builder, string model,
                      GuitarType type, Wood backWood, Wood topWood)
        {
            Guitar guitar = new Guitar(serialNumber, price, builder,
                                       model, type, backWood, topWood);
            guitars.Add(guitar);
        }

        public Guitar GetGuitar(string serialNumber)
        {
            foreach (var guitar in guitars)
            {
                if (guitar.GetSerialNumber().Equals(serialNumber))
                {
                    return guitar;
                }
            }

            return null;
        }

        public List<Guitar> Search(Guitar searchGuitar)
        {
            List<Guitar> matchingGuitars = new List<Guitar>();

            foreach (var guitar in guitars)
            {
                // Ignore serial number since that's unique
                // Ignore price since that's unique
                if (searchGuitar.GetBuilder() != guitar.GetBuilder())
                    continue;

                string model = searchGuitar.GetModel().ToLower();
                if (!string.IsNullOrEmpty(model) &&
                    !model.Equals(guitar.GetModel().ToLower()))
                    continue;

                if (searchGuitar.GetGuitarType() != guitar.GetGuitarType())
                    continue;

                if (searchGuitar.GetBackWood() != guitar.GetBackWood())
                    continue;

                if (searchGuitar.GetTopWood() != guitar.GetTopWood())
                    continue;

                matchingGuitars.Add(guitar);
            }

            return matchingGuitars;
        }
    }
}
