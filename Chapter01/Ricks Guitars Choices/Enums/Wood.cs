﻿using System.ComponentModel;

namespace RicksGuitars.Enums
{
    public enum Wood
    {
        [Description("Indian Rosewood")]
        Indian_Rosewood,

        [Description("Brazilian Rosewood")]
        Brazilian_Rosewood,

        Mahogany,
        Maple,
        Cocobolo,
        Cedar,
        Adirondack,
        Alder,
        Sitka,
    }
}
