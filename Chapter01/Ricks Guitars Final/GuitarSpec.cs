﻿using RicksGuitars.Enums;

namespace RicksGuitars
{
    public class GuitarSpec
    {
        private readonly Builder builder;

        private readonly string model;

        private readonly GuitarType type;

        private readonly int numStrings;

        private readonly Wood backWood, topWood;

        public GuitarSpec(Builder builder, string model, GuitarType type,
               int numStrings, Wood backWood, Wood topWood)
        {
            this.builder = builder;
            this.model = model;
            this.type = type;
            this.numStrings = numStrings;
            this.backWood = backWood;
            this.topWood = topWood;
        }

        public Builder GetBuilder()
        {
            return builder;
        }

        public string GetModel()
        {
            return model;
        }

        public GuitarType GetGuitarType()
        {
            return type;
        }

        public int GetNumStrings()
        {
            return numStrings;
        }

        public Wood GetBackWood()
        {
            return backWood;
        }

        public Wood GetTopWood()
        {
            return topWood;
        }

        public bool matches(GuitarSpec otherSpec)
        {
            if (builder != otherSpec.builder)
                return false;

            if (!string.IsNullOrEmpty(model) &&
                !model.ToLower().Equals(otherSpec.model.ToLower()))
                return false;

            if (type != otherSpec.type)
                return false;

            if (numStrings != otherSpec.numStrings)
                return false;

            if (backWood != otherSpec.backWood)
                return false;

            if (topWood != otherSpec.topWood)
                return false;

            return true;
        }
    }
}
