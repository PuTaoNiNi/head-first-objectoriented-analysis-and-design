﻿namespace RicksGuitars
{
    public class Guitar
    {
        private readonly string serialNumber;

        private decimal price;

        private GuitarSpec spec;

        public Guitar(string serialNumber, decimal price, GuitarSpec spec)
        {
            this.serialNumber = serialNumber;
            this.price = price;
            this.spec = spec;
        }

        public string GetSerialNumber()
        {
            return serialNumber;
        }

        public decimal GetPrice()
        {
            return price;
        }

        public void SetPrice(decimal newPrice)
        {
            this.price = newPrice;
        }

        public GuitarSpec getSpec()
        {
            return spec;
        }
    }
}
