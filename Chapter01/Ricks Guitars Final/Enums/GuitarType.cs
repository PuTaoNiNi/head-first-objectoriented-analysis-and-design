﻿namespace RicksGuitars.Enums
{
    public enum GuitarType
    {
        acoustic,
        electric,
    }
}
