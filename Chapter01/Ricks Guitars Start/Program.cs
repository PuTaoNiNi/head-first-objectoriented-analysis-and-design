﻿// See https://aka.ms/new-console-template for more information
using RicksGuitars;

// Set up Rick's guitar inventory
Inventory inventory = new Inventory();
InitializeInventory(inventory);

Guitar whatErinLikes = new Guitar("", 0, "fender", "Stratocastor",
                                  "electric", "Alder", "Alder");

Guitar guitar = inventory.Search(whatErinLikes);
if (guitar != null)
{
    Console.WriteLine("Erin, you might like this " +
      guitar.GetBuilder() + " " + guitar.GetModel() + " " +
      guitar.GetGuitarType() + " guitar:\n   " +
      guitar.GetBackWood() + " back and sides,\n   " +
      guitar.GetTopWood() + " top.\nYou can have it for only $" +
      guitar.GetPrice() + "!");
}
else
{
    Console.WriteLine("Sorry, Erin, we have nothing for you.");
}

static void InitializeInventory(Inventory inventory)
{
    inventory.AddGuitar("11277", 3999.95m, "Collings", "CJ", "acoustic",
                        "Indian Rosewood", "Sitka");

    inventory.AddGuitar("V95693", 1499.95m, "Fender", "Stratocastor", "electric",
                        "Alder", "Alder");

    inventory.AddGuitar("V9512", 1549.95m, "Fender", "Stratocastor", "electric",
                        "Alder", "Alder");

    inventory.AddGuitar("122784", 5495.95m, "Martin", "D-18", "acoustic",
                        "Mahogany", "Adirondack");

    inventory.AddGuitar("76531", 6295.95m, "Martin", "OM-28", "acoustic",
                        "Brazilian Rosewood", "Adriondack");

    inventory.AddGuitar("70108276", 2295.95m, "Gibson", "Les Paul", "electric",
                        "Mahogany", "Maple");

    inventory.AddGuitar("82765501", 1890.95m, "Gibson", "SG '61 Reissue",
                        "electric", "Mahogany", "Mahogany");

    inventory.AddGuitar("77023", 6275.95m, "Martin", "D-28", "acoustic",
                        "Brazilian Rosewood", "Adirondack");

    inventory.AddGuitar("1092", 12995.95m, "Olson", "SJ", "acoustic",
                        "Indian Rosewood", "Cedar");

    inventory.AddGuitar("566-62", 8999.95m, "Ryan", "Cathedral", "acoustic",
                        "Cocobolo", "Cedar");

    inventory.AddGuitar("6 29584", 2100.95m, "PRS", "Dave Navarro Signature",
                        "electric", "Mahogany", "Maple");
}