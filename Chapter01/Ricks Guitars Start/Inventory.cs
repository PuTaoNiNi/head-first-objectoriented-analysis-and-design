﻿namespace RicksGuitars
{
    public class Inventory
    {
        private readonly List<Guitar> guitars;

        public Inventory()
        {
            guitars = new List<Guitar>();
        }


        public void AddGuitar(string serialNumber, decimal price,
                      string builder, string model,
                      string type, string backWood, string topWood)
        {
            Guitar guitar = new Guitar(serialNumber, price, builder,
                                       model, type, backWood, topWood);
            guitars.Add(guitar);
        }

        public Guitar GetGuitar(string serialNumber)
        {
            foreach (var guitar in guitars)
            {
                if (guitar.GetSerialNumber().Equals(serialNumber))
                {
                    return guitar;
                }
            }

            return null;
        }

        public Guitar Search(Guitar searchGuitar)
        {
            foreach (var guitar in guitars)
            {
                // Ignore serial number since that's unique
                // Ignore price since that's unique
                string builder = searchGuitar.GetBuilder();
                if (!string.IsNullOrEmpty(builder) &&
                    !builder.Equals(guitar.GetBuilder()))
                    continue;

                string model = searchGuitar.GetModel();
                if (!string.IsNullOrEmpty(model) &&
                    !model.Equals(guitar.GetModel()))
                    continue;

                string type = searchGuitar.GetGuitarType();
                if (!string.IsNullOrEmpty(type) &&
                    !type.Equals(guitar.GetGuitarType()))
                    continue;

                string backWood = searchGuitar.GetBackWood();
                if (!string.IsNullOrEmpty(backWood) &&
                    !backWood.Equals(guitar.GetBackWood()))
                    continue;

                string topWood = searchGuitar.GetTopWood();
                if (!string.IsNullOrEmpty(topWood) &&
                    !topWood.Equals(guitar.GetTopWood()))
                    continue;

                return guitar;
            }

            return null;
        }
    }
}
