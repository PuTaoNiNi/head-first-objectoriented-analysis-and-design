﻿namespace RicksGuitars
{
    public class Guitar
    {
        private readonly string serialNumber, builder, model, type, backWood, topWood;

        private decimal price;

        public Guitar(string serialNumber, decimal price,
                string builder, string model, string type,
                string backWood, string topWood)
        {
            this.serialNumber = serialNumber;
            this.price = price;
            this.builder = builder;
            this.model = model;
            this.type = type;
            this.backWood = backWood;
            this.topWood = topWood;
        }

        public string GetSerialNumber()
        {
            return serialNumber;
        }

        public decimal GetPrice()
        {
            return price;
        }

        public void SetPrice(decimal newPrice)
        {
            this.price = newPrice;
        }

        public string GetBuilder()
        {
            return builder;
        }

        public string GetModel()
        {
            return model;
        }

        public string GetGuitarType()
        {
            return type;
        }

        public string GetBackWood()
        {
            return backWood;
        }

        public string GetTopWood()
        {
            return topWood;
        }
    }
}
