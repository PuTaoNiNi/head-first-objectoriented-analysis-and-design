﻿// See https://aka.ms/new-console-template for more information
using RicksGuitars;
using RicksGuitars.Enums;

// Set up Rick's guitar inventory
Inventory inventory = new Inventory();
InitializeInventory(inventory);

GuitarSpec whatErinLikes = new GuitarSpec(Builder.Fender, "Stratocastor",
                                  GuitarType.electric, Wood.Alder, Wood.Alder);

List<Guitar> matchingGuitars = inventory.Search(whatErinLikes);

if (matchingGuitars.Any())
{
    Console.WriteLine("Erin, you might like these guitars:");
    foreach (var guitar in matchingGuitars)
    {
        GuitarSpec spec = guitar.getSpec();
        Console.WriteLine("  We have a " +
        spec.GetBuilder() + " " + spec.GetModel() + " " +
        spec.GetGuitarType() + " guitar:\n   " +
        spec.GetBackWood() + " back and sides,\n   " +
        spec.GetTopWood() + " top.\n  You can have it for only $" +
        guitar.GetPrice() + "!\n  ----");
    }
}
else
{
    Console.WriteLine("Sorry, Erin, we have nothing for you.");
}

static void InitializeInventory(Inventory inventory)
{
    inventory.AddGuitar("11277", 3999.95m, Builder.Collings, "CJ", GuitarType.acoustic,
                        Wood.Indian_Rosewood, Wood.Sitka);

    inventory.AddGuitar("V95693", 1499.95m, Builder.Fender, "Stratocastor", GuitarType.electric,
                        Wood.Alder, Wood.Alder);

    inventory.AddGuitar("V9512", 1549.95m, Builder.Fender, "Stratocastor", GuitarType.electric,
                        Wood.Alder, Wood.Alder);

    inventory.AddGuitar("122784", 5495.95m, Builder.Martin, "D-18", GuitarType.acoustic,
                        Wood.Mahogany, Wood.Adirondack);

    inventory.AddGuitar("76531", 6295.95m, Builder.Martin, "OM-28", GuitarType.acoustic,
                        Wood.Brazilian_Rosewood, Wood.Adirondack);

    inventory.AddGuitar("70108276", 2295.95m, Builder.Gibson, "Les Paul", GuitarType.electric,
                        Wood.Mahogany, Wood.Maple);

    inventory.AddGuitar("82765501", 1890.95m, Builder.Gibson, "SG '61 Reissue",
                        GuitarType.electric, Wood.Mahogany, Wood.Mahogany);

    inventory.AddGuitar("77023", 6275.95m, Builder.Martin, "D-28", GuitarType.acoustic,
                        Wood.Brazilian_Rosewood, Wood.Adirondack);

    inventory.AddGuitar("1092", 12995.95m, Builder.Olson, "SJ", GuitarType.acoustic,
                        Wood.Indian_Rosewood, Wood.Cedar);

    inventory.AddGuitar("566-62", 8999.95m, Builder.Ryan, "Cathedral", GuitarType.acoustic,
                        Wood.Cocobolo, Wood.Cedar);

    inventory.AddGuitar("6 29584", 2100.95m, Builder.PRS, "Dave Navarro Signature",
                        GuitarType.electric, Wood.Mahogany, Wood.Maple);
}