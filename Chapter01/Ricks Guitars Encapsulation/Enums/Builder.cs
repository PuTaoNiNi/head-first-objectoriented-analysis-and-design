﻿namespace RicksGuitars.Enums
{
    public enum Builder
    {
        Fender,
        Martin,
        Gibson,
        Collings,
        Olson,
        Ryan,
        PRS,
    }
}
