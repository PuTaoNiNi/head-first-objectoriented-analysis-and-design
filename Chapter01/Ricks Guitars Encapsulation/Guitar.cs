﻿using RicksGuitars.Enums;

namespace RicksGuitars
{
    public class Guitar
    {
        private readonly string serialNumber;

        private decimal price;

        private GuitarSpec spec;

        public Guitar(string serialNumber, decimal price,
                Builder builder, string model, GuitarType type,
                Wood backWood, Wood topWood)
        {
            this.serialNumber = serialNumber;
            this.price = price;
            this.spec = new GuitarSpec(builder, model, type, backWood, topWood);
        }

        public string GetSerialNumber()
        {
            return serialNumber;
        }

        public decimal GetPrice()
        {
            return price;
        }

        public void SetPrice(decimal newPrice)
        {
            this.price = newPrice;
        }

        public GuitarSpec getSpec()
        {
            return spec;
        }
    }
}
