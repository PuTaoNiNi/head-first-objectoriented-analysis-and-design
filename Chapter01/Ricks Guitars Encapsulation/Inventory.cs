﻿using RicksGuitars.Enums;

namespace RicksGuitars
{
    public class Inventory
    {
        private readonly List<Guitar> guitars;

        public Inventory()
        {
            guitars = new List<Guitar>();
        }

        public void AddGuitar(string serialNumber, decimal price,
                      Builder builder, string model,
                      GuitarType type, Wood backWood, Wood topWood)
        {
            Guitar guitar = new Guitar(serialNumber, price, builder,
                                       model, type, backWood, topWood);
            guitars.Add(guitar);
        }

        public Guitar GetGuitar(string serialNumber)
        {
            foreach (var guitar in guitars)
            {
                if (guitar.GetSerialNumber().Equals(serialNumber))
                {
                    return guitar;
                }
            }

            return null;
        }

        public List<Guitar> Search(GuitarSpec searchSpec)
        {
            List<Guitar> matchingGuitars = new List<Guitar>();

            foreach (var guitar in guitars)
            {
                GuitarSpec guitarSpec = guitar.getSpec();

                // Ignore serial number since that's unique
                // Ignore price since that's unique
                if (searchSpec.GetBuilder() != guitarSpec.GetBuilder())
                    continue;

                string model = searchSpec.GetModel().ToLower();
                if (!string.IsNullOrEmpty(model) &&
                    !model.Equals(guitarSpec.GetModel().ToLower()))
                    continue;

                if (searchSpec.GetGuitarType() != guitarSpec.GetGuitarType())
                    continue;

                if (searchSpec.GetBackWood() != guitarSpec.GetBackWood())
                    continue;

                if (searchSpec.GetTopWood() != guitarSpec.GetTopWood())
                    continue;

                matchingGuitars.Add(guitar);
            }

            return matchingGuitars;
        }
    }
}
