﻿using RicksGuitars.Enums;

namespace RicksGuitars
{
    public class GuitarSpec
    {
        private readonly Builder builder;

        private readonly string model;

        private readonly GuitarType type;

        private readonly Wood backWood, topWood;

        public GuitarSpec(Builder builder, string model, GuitarType type,
               Wood backWood, Wood topWood)
        {
            this.builder = builder;
            this.model = model;
            this.type = type;
            this.backWood = backWood;
            this.topWood = topWood;
        }

        public Builder GetBuilder()
        {
            return builder;
        }

        public string GetModel()
        {
            return model;
        }

        public GuitarType GetGuitarType()
        {
            return type;
        }

        public Wood GetBackWood()
        {
            return backWood;
        }

        public Wood GetTopWood()
        {
            return topWood;
        }
    }
}
